package com.epam.guesstheword;

import java.io.*;
import java.util.*;


public class WordProcess implements WordReader {
    List<String> wordsList;
    List<String> wordsListPlayedInGame;

    public WordProcess() {
        wordsList = readWords("words.txt");
        wordsListPlayedInGame = new ArrayList<>();
    }

    @Override
    public List<String> readWords(String file) {
        List<String> wordsList = new ArrayList<String>();
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(new File(file)));
            String read;
            while ((read = bufferedReader.readLine()) != null) {
                wordsList.add(read);
            }
        } catch (FileNotFoundException e) {
            System.out.println("File not found.");
        } catch (IOException e) {
            System.out.println("I/O error.");
        }
        return wordsList;
    }

    public int getWordsNumber() {
        return wordsList.size();
    }

    public String getRandomWord() {
        Collections.shuffle(wordsList);
        String randomWord = wordsList.get(0);
        wordsList.remove(randomWord);
        return randomWord;
    }

    public String encodeWord(String word) {
        return word.replaceAll(".", "*");
    }

    public boolean isWordGuessed(String word, List<String> letters) {
        Set<String> set = new HashSet<>(letters);
        for (int i = 0; i < word.length(); i++) {
            if (!set.contains(Character.toString(word.charAt(i)))) {
                return false;
            }
        }
        return true;
    }

    public String encryptWord(String word, List<String> letters) {

        String encodedWord = new WordProcess().encodeWord(word);
        String[] encodedCharacters = encodedWord.split("");
        String[] array = new String[letters.size()];
        array = letters.toArray(array);
        for (int j = 0; j < array.length; j++) {
            int index = word.indexOf(array[j]);
            while (index != -1) {
                encodedCharacters[index] = array[j];
                index = word.indexOf(array[j], index + 1);
            }
        }
        String result = Arrays.toString(encodedCharacters);
        result = result.substring(1, result.length() - 1)
                .replace(",", "")
                .replaceAll(" ", "");
        return result;
    }

    public boolean isWordContains(String word, String letter) {
        return word.contains(letter);
    }
}


