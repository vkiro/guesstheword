package com.epam.guesstheword;

import java.util.List;

public interface WordReader {
    List<String> readWords(String file);
}
