package com.epam.guesstheword;

import java.util.ArrayList;
import java.util.List;

public class Player {
    private String name;
    private int earnPoints;
    private int lostPoints;
    private List<String> guessedWords = new ArrayList<>();
    private List<String> guessedLetters = new ArrayList<>();
    private boolean isAlive;
    private int lives = 5;

    public Player(String name) {
        this.name = name;
    }

    public void minusPoint() {
        lives--;
    }

    public boolean isLive() {
        return lives > 0;
    }

    public int getLives() {
        return lives;
    }

    public void addLetter(String letter) {
        guessedLetters.add(letter);
    }

    public List<String> getLetters() {
        return guessedLetters;
    }

    public void addGuessedWord(String word) {
        guessedWords.add(word);
    }

    public List<String> getGuessedWords() {
        return guessedWords;
    }

    public int addEarnPoints() {
        earnPoints++;
        return earnPoints;
    }

    public int addLostPoints() {
        lostPoints++;
        return lostPoints++;
    }

    public void resetLetters() {
        this.guessedLetters = new ArrayList<>();
    }

    public void resetLife() {
        this.lives = 5;
    }
}
