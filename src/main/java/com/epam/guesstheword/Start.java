package com.epam.guesstheword;

import com.epam.guesstheword.model.GameProcess;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Start {

    public static void main(String[] args) throws IOException {
        GameProcess gameProcess = new GameProcess();
        gameProcess.showRulesOfTheGame();
        gameProcess.showMenu();
        gameProcess.game();
    }
}
