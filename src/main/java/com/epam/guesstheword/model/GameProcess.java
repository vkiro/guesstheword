package com.epam.guesstheword.model;

import com.epam.guesstheword.Console;
import com.epam.guesstheword.Player;
import com.epam.guesstheword.WordProcess;

import java.io.IOException;
import java.sql.SQLOutput;
import java.util.List;
import java.util.Scanner;

public class GameProcess {
    private Scanner scanner = new Scanner(System.in);
    private Messages messages = new Messages();


    public void showRulesOfTheGame() {
        System.out.println(messages.getRulesOfTheGame());
    }


    public String enterLetter() throws IOException {
        System.out.println(messages.getAskToEnterLetter());
        String letter = scanner.nextLine();
        Scanner scanner1 = new Scanner(letter);
        if (scanner1.hasNext("[A-z]")) {
            return letter;
        } else if (letter.equals("menu")) {
            System.out.println(messages.getMenu());
            String command = scanner.nextLine();
            goToMenu(command);
            return enterLetter();
        } else {
            System.out.println(messages.getWrongCommand());
            return enterLetter();
        }
    }

    public void showMenu() {
        System.out.println();
    }

    public void goToMenu(String command) throws IOException {
        switch (command) {
            case "exit":
                System.exit(0);
            case "restart":
                game();
                System.exit(0);
            case "rule":
                showRulesOfTheGame();
                break;
            case "back":
                break;
            default:
                System.out.println(messages.getWrongCommand());
                String nextCommand = scanner.nextLine();
                goToMenu(nextCommand);
        }
    }

    public void showLoseMessage(String word) {
        System.out.println(messages.getDidNotGuess() + word);
    }

    public void showGuessedMessage(String word) {
        System.out.println(messages.getGuessed() + word);
    }

    public void game() throws IOException {
        WordProcess wordProcess = new WordProcess();
        Console console = new Console();
        System.out.println(messages.getAskEnterNamePlayer());
        String playerName = console.read();
        Player player = new Player(playerName);
        for (int number = 0; number <= wordProcess.getWordsNumber(); number++) {
            String word = wordProcess.getRandomWord();
            while (player.isLive() && (!wordProcess.isWordGuessed(word, player.getLetters()))) {
                console.write(wordProcess.encryptWord(word, player.getLetters()));
                String letter = enterLetter();
                if ((!wordProcess.isWordContains(word, letter)) && (!player.getLetters().contains(letter))) {
                    player.minusPoint();
                    console.write(messages.getLostLife());
                    console.write(messages.getPlayerLifes() + Messages.ANSI_CYAN + player.getLives() + Messages.ANSI_RESET);
                }
                player.addLetter(letter);
            }
            if (wordProcess.isWordGuessed(word, player.getLetters())) {
                player.addGuessedWord(word);
                showGuessedMessage(word);
            } else {
                showLoseMessage(word);
            }
            player.resetLetters();
            player.resetLife();
            console.write(messages.getListWords() + showWords(player.getGuessedWords()));
            console.write(messages.getNewWord());
        }
    }

    public String showWords(List<String> words) {
        StringBuilder allWords = new StringBuilder();
        for (String word : words) {
            allWords.append(word).append(", ");
        }
        return allWords.toString();
    }
}



