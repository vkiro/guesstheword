package com.epam.guesstheword.model;

public final class Messages {
    public static final String ANSI_RESET = "\u001B[0m";

    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_WHITE = "\u001B[37m";
    public static final String newLine = System.getProperty("line.separator");
    private String rulesOfTheGame = "The word to guess is represented by a row of dashes, representing each letter of the word." +
            newLine +
            "In most variants, proper nouns, such as names, places, and brands, are not allowed." +
            newLine +
            " Slang words, sometimes referred to as informal or shortened words, are also not allowed." +
            newLine +
            " If the guessing player suggests a letter which occurs in the word, the letter is written  in all its correct positions." +
            newLine +
            "If the suggested letter  does not occur in the word, the  player lose one life" +
            newLine +
            "The player will win if he guess all letter of the word." +
            newLine +
            "Game over when player lose 5 life." +
            newLine +
            "Enter " +
            ANSI_YELLOW +
            "menu " +
            ANSI_RESET +
            " for shows menu game!" +
            ANSI_CYAN +
            newLine +
            "Good LUCK!" +
            ANSI_RESET;

    private String askToEnterLetter = "Enter the letter you want to open:";
    private String wrongCommand = ANSI_RED + "You entered wrong command. Try again!" + ANSI_RESET;
    private String menu = ANSI_YELLOW +
            "exit" +
            ANSI_RESET +
            "- finish game" +
            newLine +
            ANSI_YELLOW +
            "restart " +
            ANSI_RESET +
            "- start new game" +
            newLine +
            ANSI_YELLOW +
            "rule " +
            ANSI_RESET +
            "- show rule of the game" +
            newLine +
            ANSI_YELLOW +
            "back " +
            ANSI_RESET +
            "- return to game";
    private String askEnterNamePlayer = "Enter your name:";
    private String didNotGuess = ANSI_RED + "Sorry, but you don't guess  word! It`s " + ANSI_RESET;
    private String guessed = ANSI_GREEN + "Congratulations! You opened all letter of word! It`s " + ANSI_RESET;
    private String newWord = newLine + "New Word: ";
    private String playerLifes = "Your life: ";
    private String lostLife = "You lost one life.";
    private String listWords = ANSI_BLUE + "Words are opened: " + ANSI_RESET;


    public String getNewWord() {
        return newWord;
    }

    public String getLostLife() {
        return lostLife;
    }

    public String getPlayerLifes() {
        return playerLifes;
    }

    public String getListWords() {
        return listWords;
    }

    public String getRulesOfTheGame() {
        return rulesOfTheGame;
    }

    public String getAskToEnterLetter() {
        return askToEnterLetter;
    }

    public String getWrongCommand() {
        return wrongCommand;
    }

    public String getMenu() {
        return menu;
    }

    public String getAskEnterNamePlayer() {
        return askEnterNamePlayer;
    }

    public String getDidNotGuess() {
        return didNotGuess;
    }

    public String getGuessed() {
        return guessed;
    }
}
